# Curso de Automatización Web Screenplay
El proyecto es un curso donde se realizó automatizaciones web, su metodología de desarrollo es Screenplay y se realizó en las siguientes plataformas: 

| Plataformas |Links|
| ------ | ------ |
| **Choucair Testing:**| `https://operacion.choucairtesting.com/academy/login/index.php`.|
| **Automación Demo Site:** |`http://demo.automationtesting.in/Register.html`.|
| **Herokuapp:** |`http://automatizacion.herokuapp.com/pperez/`.|
|**OrangeHRM:** |`https://orangehrm-demo-6x.orangehrmlive.com/`.|
| **Translate Google:** |`https://translate.google.com/?hl=es`.|

En cada uno de los respectivos `.feature` encontraras unos comentarios las cuales son:
 - **1-** Encontraras las diferentes opciones separados por un slash "/" de cada campo, aunque no están todas las opciones.
 - **2-** También hay Sugerencias o precondiciones para la ejecución.

## Tecnologías Implementadas
| Tecnologías |Nombre | Versión|
| ------ | ------ |------|
| Lenguaje |Java|8_251|
|Management Tool|Gradle|6.1.1|
|Framework Test|SerenityBDD|2.0.80|
|Software tool|Serenity-Cucumber|1.9.51|

## 1) Choucair Testing
Se automatizo un caso de prueba, la cual correspondía a:
 - Validar el ingreso exitoso a un curso en la plataforma.

| Precondiciones |
| ------ |
|- Tienen que poner un usuario y una contraseña valida. |
|- El nombre del curso debe ser exacto como esta en la plataforma. |

## 2) Automación Demo Site
Se automatizo un caso de prueba, la cual correspondía a:
- Verificar el registro exitoso de los datos en un formulario de la plataforma.

| Precondiciones |
| ------ |
|- Los campos de email y phone, sus valores no se pueden repetir son únicos |

## 3) Automación Demo Site
Se automatizo tres casos de pruebas, la cual corresponden a:
- Validar el registro exitoso de un médico.
- Validar el registro exitoso de un paciente.
- Validar el registro exitoso de una cita con el doctor y el paciente.

| Precondiciones |
| ------ |
|- El nombre y el apellido son obligatorio. |
|- El campo documentNum, este debe tener un valor único. |
|- Se deben usar fechas mayores a la fecha actual Month/Day/Year. |
|- Tanto el doctor como el paciente, deben estar previamente creados. |

## 4) OrangeHRM
Se automatizo un caso de prueba, la cual corresponden a:
- Validar el registro exitoso de un empleado.

| Precondiciones |
| ------ |
|- Todos los elementos que despliegan una lista no se pueden controlar de forma normal. |
|- Se Pueden controlar, pero mapeando cada ítem de la lista y con una condicional seleccionarla. |


## 5) Translate Google
Se automatizo un caso de prueba, la cual corresponden a:
- Validar la traducción de una palabra, en un idioma a otro.

| Precondiciones |
| ------ |
|- Puedes usar cualquier idioma, asegúrate que este bien escrito. |
|- Puedes usar cualquier palabra, asegúrate a copiar bien la traducción. |

## Ejecución por Terminal o Consola
La ejecución por consola trae ventajas al momento de probar.
- Nos permite ejecutar las pruebas sin necesidad de un IDE.
- También el tema de integración continua se hace más fácil su configuración.
- Entre muchas otras características.

### Instrucciones 
- **PASO 1:** Ubicado dentro de la carpeta principal del proyecto `ScreenplayWeb`, abrimos la terminal en esta ubicación o ruta.
- **PASO 2:** Escribimos el siguiente comando `$ gradle clean test aggregate`, luego ejecutamos.
- **PASO 3:** Esperamos que las pruebas se completen.
- **PASO 4:** Buscamos el index.html `target\site\serenity\index.html`, y lo abrimos con un navegador.
