Feature: Validate a registration on Demo Site

  @Registration
  Scenario Outline: Make a registration on Demo Site

    Given That Carlos is located on Demo Site
    When He makes the registration on the page
      | nameImg   | firstName   | lastName   | address   | email   | phone   | gender   | hobby   | language   | skills   | country   | continent   | dateOfBirth   | password   |
      | <nameImg> | <firstName> | <lastName> | <address> | <email> | <phone> | <gender> | <hobby> | <language> | <skills> | <country> | <continent> | <dateOfBirth> | <password> |
    Then He verifies that the registration was done

    Examples:
      | nameImg  | firstName | lastName | address         | email                 | phone      | gender | hobby  | language | skills | country  | continent | dateOfBirth   | password      |
      | file.png | Camilo    | peres    | calle 33 # 44-2 | cperes100@example.com | 1259891345 | Male   | Movies | Italian  | Java   | Colombia | Japan     | 1933-April-15 | Choucair12345 |

      # gender : Male / FeMale
      # hobby  : Cricket / Movies / Hockey
      # language : English / Filipino / French / Italian  / Persian
      # skills : Android / APIs / AutoCAD / Corel Draw / Excel
      # country: Angola / Bolivia / Canada /  Dominica /  Egypt
      # continent: Australia / Hong Kong / New Zealand / South Africa / United States of America

      #Los campos de email y phone, sus valores no se pueden repetir son unicos.
