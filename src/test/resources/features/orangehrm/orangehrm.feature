Feature: Validate the registration of the OrangeHRM platform

  @RegisterEmployee
  Scenario Outline: Register employee

    Given That Carlos is located on the OrangeHRM
    When He registers the employee on the platform
      | firstName   | middleName   | lastName   | location   | otherId   | birthday   | maritalStatus   | gender   | nationality   | driverLicense   | licenceExpiry   | nickName   | militaryService   | smoke   | hobby   |
      | <firstName> | <middleName> | <lastName> | <location> | <otherId> | <birthday> | <maritalStatus> | <gender> | <nationality> | <driverLicense> | <licenceExpiry> | <nickName> | <militaryService> | <smoke> | <hobby> |
    Then He verifies that the employee was registered
      | firstName   | middleName   | lastName   |
      | <firstName> | <middleName> | <lastName> |

    Examples:
      | firstName | middleName | lastName | location   | otherId   | birthday         | maritalStatus | gender     | nationality | driverLicense | licenceExpiry    | nickName     | militaryService | smoke | hobby      |
      | Lucia     | Test_01    | Peres    | Australian | 444444441 | Wed, 17 Jul 1968 | Other         | Non-Binary | Bolivian    | 37923693      | Fri, 28 May 2021 | PachiPruebas | SI              | Si    | PlayFutbol |

    # location : Canadian / Australian / Bangladesh / China
    # maritalStatus : Married / Single / Other
    # gender : Female / Male / Non-Binary
    # nationality : American / Argentinean / Australian / Bolivian / French
    # smoke :  Si / No

    # Todos los elementos que despliegan una lista no se pueden controlar de forma normal.
    # Se Pueden controlar pero mapeando cada ítem de la lista y con una condicional seleccionarla.