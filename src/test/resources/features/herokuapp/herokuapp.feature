@HerokuappAll
Feature: Validate functionalities on the Herokuapp platform

  Background:
    Given That Carlos is located on the Herokuapp platform

  @RegistrationDoctor
  Scenario Outline: Validate a doctor's registration

    When He registers the doctor on the platform
      | firstName   | lastName   | phone   | documentType   | documentNum   |
      | <firstName> | <lastName> | <phone> | <documentType> | <documentNum> |
    Then He verifies that the doctor was registered <state>

    Examples:
      | firstName | lastName | phone      | documentType         | documentNum | state     |
      | Juan      | Sures    | 5555555553 | Cédula de ciudadanía | 2211111111  | Guardado: |

    # El nombre y el apellido son obligatorio.
    # El campo documentNum, este debe tener un valor único.


  @RegistrationPatient
  Scenario Outline: Validate a patient registration

    When He registers the patient on the platform
      | firstName   | lastName   | phone   | documentType   | documentNum   |
      | <firstName> | <lastName> | <phone> | <documentType> | <documentNum> |
    Then He verifies that the patient was registered <state>

    Examples:
      | firstName | lastName | phone      | documentType         | documentNum | state     |
      | Andres    | Kaceres  | 4444444444 | Cédula de ciudadanía | 3311111117  | Guardado: |

    # El nombre y el apellido son obligatorio.
    # El campo documentNum, este debe tener un valor único.


  @MakeAnAppointment
  Scenario Outline: Validate the registration of the appointment

    When He makes an appointment
      | date   | documentNumDoctor   | comments   | documentNumPatient   |
      | <date> | <documentNumDoctor> | <comments> | <documentNumPatient> |
    Then He verifies that the appointment was created <state>

    Examples:
      | date       | documentNumDoctor | documentNumPatient | comments          | state     |
      | 12/30/2020 | 2211111111        | 3311111111         | Esta pendiente !! | Guardado: |

    # Se deben usar fechas mayores a la fecha actual Month/Day/Year.
    # Tanto el doctor como el paciente, deben estar previamente creados.










