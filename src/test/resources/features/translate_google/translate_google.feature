Feature:  Validate the functionality on the Translation Google platform

  @TranslateWord
  Scenario Outline: Translate word from one language into another

    Given That Carlos is located on the translation google platform
    When He translates the word
      | languageInput   | languageOutput   | wordInput   |
      | <languageInput> | <languageOutput> | <wordInput> |
    Then He verifies that the translation is correct <wordOutput>

    Examples:
      | languageInput | languageOutput | wordInput | wordOutput |
      | inglés        | árabe          | table     | الطاولة    |

    # languageInput  : alemán / chino / francés /español / árabe / inglés
    # languageOutput : alemán / chino / francés /español / árabe / inglés

    # Puedes usar cualquier idioma, asegúrate que este bien escrito.
    # Puedes usar cualquier palabra, asegúrate a copiar bien la traducción.