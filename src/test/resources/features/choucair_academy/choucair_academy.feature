Feature: Validate the entry to a course on the Choucair Academy platform

  @ChoucairAcademy
  Scenario Outline: Enter the web automation course

    Given That Carlos is on the Choucair Academy platform
      | userName   | password   |
      | <userName> | <password> |
    When He looks for the course <course> in the platform of Choucair Academy
    Then He Verifies that you have successfully entered the Course <course>

    Examples:
      | userName | password | course                                    |
      |          |          | Cucumber - SerenityBDD + Screenplay (WEB) |

    # Tienen que poner un usuario y una contraseña valida.
    # El nombre del curso debe ser exacto como esta en la plataforma.