package com.screenplay.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/demo_site/demo_site.feature",
        tags = "@Registration",
        glue = {"com.screenplay.steps_definitions.demo_site", "com.screenplay.utils"},
        snippets = SnippetType.CAMELCASE
)
public class RunnerDemoSide {
}
