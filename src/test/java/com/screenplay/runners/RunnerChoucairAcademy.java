package com.screenplay.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/choucair_academy/choucair_academy.feature",
        tags = "@ChoucairAcademy",
        glue = {"com.screenplay.steps_definitions.choucair_academy", "com.screenplay.utils"},
        snippets = SnippetType.CAMELCASE
)
public class RunnerChoucairAcademy {
}
