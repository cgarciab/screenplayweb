package com.screenplay.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/orangehrm/orangehrm.feature",
        tags = "@RegisterEmployee",
        glue = {"com.screenplay.steps_definitions.orangehrm", "com.screenplay.utils"},
        snippets = SnippetType.CAMELCASE
)
public class RunnerOrangeHrm {
}
