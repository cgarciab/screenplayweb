package com.screenplay.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/translate_google/translate_google.feature",
        tags = "@TranslateWord",
        glue = {"com.screenplay.steps_definitions.translate_google", "com.screenplay.utils"},
        snippets = SnippetType.CAMELCASE
)
public class RunnerTranslateGoogle {
}
