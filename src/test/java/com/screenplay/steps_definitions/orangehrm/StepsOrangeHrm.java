package com.screenplay.steps_definitions.orangehrm;

import com.screenplay.models.orangehrm.ModelOrangeHrm;
import com.screenplay.questions.orangehrm.QuestionVerifyRegisterEmployee;
import com.screenplay.tasks.orangehrm.TaskEnterOnPlatform;
import com.screenplay.tasks.orangehrm.TaskFindEmployee;
import com.screenplay.tasks.orangehrm.TaskRegisterEmployee;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsOrangeHrm {

    @Given("^That Carlos is located on the OrangeHRM$")
    public void thatCarlosIsLocatedOnTheOrangeHRM() {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                TaskEnterOnPlatform.enterOn()
        );
    }

    @When("^He registers the employee on the platform$")
    public void heRegistersTheEmployeeOnThePlatform(List<ModelOrangeHrm> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskRegisterEmployee.registerEmployee(data),
                TaskFindEmployee.findEmployee(data)
        );
    }

    @Then("^He verifies that the employee was registered$")
    public void heVerifiesThatTheEmployeeWasRegistered(List<ModelOrangeHrm> data) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyRegisterEmployee.isRegistered(data), equalTo(true))
        );
    }
}
