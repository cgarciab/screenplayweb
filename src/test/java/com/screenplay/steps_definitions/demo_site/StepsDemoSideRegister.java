package com.screenplay.steps_definitions.demo_site;

import com.screenplay.models.demo_side.ModelRegisterDemoSide;
import com.screenplay.questions.demo_side.QuestionVerifyRegister;
import com.screenplay.tasks.demo_side.TaskFillInTheForm;
import com.screenplay.userinterfaces.demo_side.PageObjectDemoSideRegister;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsDemoSideRegister {

    private PageObjectDemoSideRegister pageObjectDemoSideRegister;

    @Given("^That Carlos is located on Demo Site$")
    public void thatCarlosIsLocatedOnDemoSite() {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                Open.browserOn(pageObjectDemoSideRegister)
        );
    }

    @When("^He makes the registration on the page$")
    public void heMakesTheRegistrationOnThePage(List<ModelRegisterDemoSide> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskFillInTheForm.fillInTheFields(data)
        );
    }

    @Then("^He verifies that the registration was done$")
    public void heVerifiesThatTheRegistrationWasDone() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyRegister.isActiveTagWebTable(), equalTo(true))
        );
    }
}
