package com.screenplay.steps_definitions.translate_google;

import com.screenplay.models.traductor.ModelTranslateGoogle;
import com.screenplay.questions.traductor.QuestionVerifyTranslatedWord;
import com.screenplay.tasks.traductor.TaskTranslateGoogleWord;
import com.screenplay.userinterfaces.traductor.PageObjectTranslateGoogle;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsTranslateGoogle {

    private PageObjectTranslateGoogle pageObjectTranslateGoogle;

    @Given("^That Carlos is located on the translation google platform$")
    public void thatCarlosIsLocatedOnThTranslationGooglePlatform() {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                Open.browserOn(pageObjectTranslateGoogle)
        );
    }

    @When("^He translates the word$")
    public void HeTranslatesTheWord(List<ModelTranslateGoogle> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskTranslateGoogleWord.translateWord(data)
        );
    }

    @Then("^He verifies that the translation is correct (.*)$")
    public void heVerifiesThatTheTranslationIsCorrect(String wordOutPut) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyTranslatedWord.OutputWordIs(wordOutPut), equalTo(true))
        );
    }
}
