package com.screenplay.steps_definitions.choucair_academy;

import com.screenplay.models.choucair_academy.ModelChoucairAcademy;
import com.screenplay.questions.choucair_academy.QuestionVerifyEnterCourse;
import com.screenplay.tasks.choucair_academy.TaskLoginUniversityChoucair;
import com.screenplay.tasks.choucair_academy.TaskOpenUpPlatform;
import com.screenplay.tasks.choucair_academy.TaskSearchCourse;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

public class StepsChoucairAcademyCourse {

    @Given("^That Carlos is on the Choucair Academy platform$")
    public void ThatCarlosIsOnTheChoucairAcademyPlatform(List<ModelChoucairAcademy> data) {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                TaskOpenUpPlatform.enterThePage(),
                TaskLoginUniversityChoucair.signIn(data)
        );
    }

    @When("^He looks for the course (.*) in the platform of Choucair Academy$")
    public void HeLooksForTheCourseInThePlatformOfChoucairAcademy(String curse) {
        OnStage.theActorInTheSpotlight().attemptsTo(TaskSearchCourse.findTheCourse(curse));
    }

    @Then("^He Verifies that you have successfully entered the Course (.*)$")
    public void HeVerifiesThatYouHaveSuccessfullyEnteredTheCourse(String curse) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(QuestionVerifyEnterCourse.verifyTheCourse(curse))
        );
    }
}
