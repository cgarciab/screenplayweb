package com.screenplay.steps_definitions.herokuapp;

import com.screenplay.models.herokuapp.ModelHerokuAppointment;
import com.screenplay.questions.herokuapp.QuestionVerifyRegister;
import com.screenplay.tasks.herokuapp.TaskHerokuappMakeAppointment;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsHerokuappMakeAppointment {

    @When("^He makes an appointment$")
    public void heMakesAnAppointment(List<ModelHerokuAppointment> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskHerokuappMakeAppointment.makeAnAppointment(data)
        );
    }

    @Then("^He verifies that the appointment was created (.*)$")
    public void heVerifiesThatTheAppointmentWasCreated(String status) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyRegister.isSaved(status), equalTo(true))
        );
    }
}
