package com.screenplay.steps_definitions.herokuapp;

import com.screenplay.models.herokuapp.ModelHerokuRegister;
import com.screenplay.questions.herokuapp.QuestionVerifyRegister;
import com.screenplay.tasks.herokuapp.TaskHerokuappRegisterDoctor;
import com.screenplay.userinterfaces.herokuapp.PageObjectHerokuapp;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsHerokuappRegisterDoctor {

    private PageObjectHerokuapp pageObjectHerokuapp;

    @Given("^That Carlos is located on the Herokuapp platform$")
    public void thatCarlosIsLocatedOnTheHerokuappPlatform() {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                Open.browserOn(pageObjectHerokuapp)
        );
    }

    @When("^He registers the doctor on the platform$")
    public void heRegistersTheDoctorOnThePlatform(List<ModelHerokuRegister> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskHerokuappRegisterDoctor.enterData(data)
        );
    }

    @Then("^He verifies that the doctor was registered (.*)$")
    public void heVerifiesThatTheDoctorWasRegistered(String status) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyRegister.isSaved(status), equalTo(true))
        );
    }
}
