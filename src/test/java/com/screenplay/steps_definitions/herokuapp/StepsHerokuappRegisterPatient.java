package com.screenplay.steps_definitions.herokuapp;

import com.screenplay.models.herokuapp.ModelHerokuRegister;
import com.screenplay.questions.herokuapp.QuestionVerifyRegister;
import com.screenplay.tasks.herokuapp.TaskHerokuappRegisterPatient;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepsHerokuappRegisterPatient {

    @When("^He registers the patient on the platform$")
    public void heRegistersThePatientOnThePlatform(List<ModelHerokuRegister> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                TaskHerokuappRegisterPatient.enterData(data)
        );
    }

    @Then("^He verifies that the patient was registered (.*)$")
    public void heVerifiesThatThePatientWasRegistered(String status) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(QuestionVerifyRegister.isSaved(status), equalTo(true))
        );
    }
}
