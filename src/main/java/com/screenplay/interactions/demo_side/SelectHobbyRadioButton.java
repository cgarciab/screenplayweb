package com.screenplay.interactions.demo_side;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import java.util.List;

public class SelectHobbyRadioButton implements Interaction {
    private final List<WebElementFacade> webElementFacade;
    private final String HOBBY;
    private WebElementFacade optionWebElement;

    public SelectHobbyRadioButton(List<WebElementFacade> webElementFacade, String option) {
        this.webElementFacade = webElementFacade;
        this.HOBBY = option;
    }

    public static Performable selectItem(List<WebElementFacade> resolveAllFor, String option) {
        return Tasks.instrumented(SelectHobbyRadioButton.class, resolveAllFor, option);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade())
        );
    }

    private WebElementFacade getWebElementFacade() {
        for (int i = 0; i < webElementFacade.size(); ++i) {
            if (webElementFacade.get(i).getValue().equals(HOBBY)) {
                optionWebElement = webElementFacade.get(i);
                i += webElementFacade.size() + 1;
            }
        }
        return optionWebElement;
    }
}
