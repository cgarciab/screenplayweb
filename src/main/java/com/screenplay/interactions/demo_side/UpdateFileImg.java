package com.screenplay.interactions.demo_side;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.nio.file.Paths;

public class UpdateFileImg implements Interaction {

    private final String NAME_IMG;

    public UpdateFileImg(String nameImg) {
        this.NAME_IMG = nameImg;
    }

    public static Performable fileInput(String nameImg) {
        return Tasks.instrumented(UpdateFileImg.class, nameImg);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
        String pathImg = Paths.get("").toAbsolutePath().toString() + "\\src\\test\\resources\\img\\";
        WebElement webElement = driver.findElement(By.xpath("//input[@id='imagesrc']"));
        webElement.sendKeys(pathImg + NAME_IMG);
    }
}
