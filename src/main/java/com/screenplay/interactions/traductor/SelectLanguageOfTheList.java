package com.screenplay.interactions.traductor;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import java.util.List;

public class SelectLanguageOfTheList implements Interaction {
    private final List<WebElementFacade> webElementFacade;
    private final String optionItem;
    private WebElementFacade optionWebElement;

    public SelectLanguageOfTheList(List<WebElementFacade> webElementFacade, String optionItem) {
        this.webElementFacade = webElementFacade;
        this.optionItem = optionItem;
    }

    public static Performable selectItem(List<WebElementFacade> resolveAllFor, String optionItem) {
        return Tasks.instrumented(SelectLanguageOfTheList.class, resolveAllFor, optionItem);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade())
        );
    }

    private WebElementFacade getWebElementFacade() {
        for (int i = 1; i < webElementFacade.size(); ++i) {
            if (webElementFacade.get(i).getText().equals(optionItem)) {
                optionWebElement = webElementFacade.get(i);
                i += webElementFacade.size() + 1;
            }
        }
        return optionWebElement;
    }
}
