package com.screenplay.interactions.orangehrm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormTwo.*;

public class SelectGenderComboBox implements Interaction {
    private final String GENDER;
    private WebElementFacade optionWebElement;

    public SelectGenderComboBox(String gender) {
        this.GENDER = gender;
    }

    public static Performable selectGender(String gender) {
        return Tasks.instrumented(SelectGenderComboBox.class, gender);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade(actor))
        );
    }

    private WebElementFacade getWebElementFacade(Actor actor) {
        if (GENDER.equalsIgnoreCase("Female")) {
            optionWebElement = OPTION_GENDER_FEMALE.resolveFor(actor);
        } else if (GENDER.equalsIgnoreCase("Male")) {
            optionWebElement = OPTION_GENDER_MALE.resolveFor(actor);
        } else if (GENDER.equalsIgnoreCase("Non-Binary")) {
            optionWebElement = OPTION_GENDER_NONBINARY.resolveFor(actor);
        }
        return optionWebElement;
    }
}
