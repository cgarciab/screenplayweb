package com.screenplay.interactions.orangehrm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;

public class WaitAWhile implements Interaction {

    private int time;

    public WaitAWhile(int time) {
        this.time = time;
    }

    public static Performable waitSeconds(int time) {
        return Tasks.instrumented(WaitAWhile.class, time);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
