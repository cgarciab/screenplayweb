package com.screenplay.interactions.orangehrm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormTwo.CCKBOX_SMOKE;

public class SelectSmokeCheckBox implements Interaction {

    private final String SMOKE;

    public SelectSmokeCheckBox(String smoke) {
        this.SMOKE = smoke;
    }

    public static Performable selectOption(String smoke) {
        return Tasks.instrumented(SelectSmokeCheckBox.class, smoke);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (SMOKE.equalsIgnoreCase("si") || SMOKE.equalsIgnoreCase("yes") || SMOKE.equalsIgnoreCase("true")) {
            actor.attemptsTo(
                    Click.on(CCKBOX_SMOKE)
            );
        }
    }
}
