package com.screenplay.interactions.orangehrm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormTwo.*;

public class SelectMaritalStatusComboBox implements Interaction {

    private final String maritalStatus;
    private WebElementFacade optionWebElement;

    public SelectMaritalStatusComboBox(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public static Performable selectMaritalStatus(String maritalStatus) {
        return Tasks.instrumented(SelectMaritalStatusComboBox.class, maritalStatus);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade(actor))
        );
    }

    private WebElementFacade getWebElementFacade(Actor actor) {
        if (maritalStatus.equalsIgnoreCase("Married")) {
            optionWebElement = OPTION_MARRIED_MARITAL.resolveFor(actor);
        } else if (maritalStatus.equalsIgnoreCase("Single")) {
            optionWebElement = OPTION_SINGLE_MARITAL.resolveFor(actor);
        } else if (maritalStatus.equalsIgnoreCase("Other")) {
            optionWebElement = OPTION_OTHER_MARITAL.resolveFor(actor);
        }
        return optionWebElement;
    }
}
