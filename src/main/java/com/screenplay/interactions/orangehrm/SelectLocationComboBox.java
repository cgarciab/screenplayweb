package com.screenplay.interactions.orangehrm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormOne.*;

public class SelectLocationComboBox implements Interaction {

    private final String location;
    private WebElementFacade optionWebElement;

    public SelectLocationComboBox(String location) {
        this.location = location;
    }

    public static Performable selectOptionLocation(String location) {
        return Tasks.instrumented(SelectLocationComboBox.class, location);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade(actor))
        );
    }

    private WebElementFacade getWebElementFacade(Actor actor) {
        if (location.equalsIgnoreCase("Canadian")) {
            optionWebElement = OPTION_CANADIAN_LOCATION.resolveFor(actor);
        } else if (location.equalsIgnoreCase("Australian")) {
            optionWebElement = OPTION_AUSTRALIAN_LOCATION.resolveFor(actor);
        } else if (location.equalsIgnoreCase("Bangladesh")) {
            optionWebElement = OPTION_BANGLADESH_LOCATION.resolveFor(actor);
        } else if (location.equalsIgnoreCase("China")) {
            optionWebElement = OPTION_CHINA_LOCATION.resolveFor(actor);
        }
        return optionWebElement;
    }
}
