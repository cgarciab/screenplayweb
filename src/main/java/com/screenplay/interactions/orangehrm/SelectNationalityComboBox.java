package com.screenplay.interactions.orangehrm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormTwo.*;

public class SelectNationalityComboBox implements Interaction {
    private final String NATIONALITY;
    private WebElementFacade optionWebElement;

    public SelectNationalityComboBox(String nationality) {
        this.NATIONALITY = nationality;
    }

    public static Performable selectNationality(String nationality) {
        return Tasks.instrumented(SelectNationalityComboBox.class, nationality);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(this.getWebElementFacade(actor))
        );
    }

    private WebElementFacade getWebElementFacade(Actor actor) {
        if (NATIONALITY.equalsIgnoreCase("American")) {
            optionWebElement = OPTION_AMERICAN_NATIONALITY.resolveFor(actor);
        } else if (NATIONALITY.equalsIgnoreCase("Argentinean")) {
            optionWebElement = OPTION_ARGENTINEAN_NATIONALITY.resolveFor(actor);
        } else if (NATIONALITY.equalsIgnoreCase("Australian")) {
            optionWebElement = OPTION_AUSTRALIAN_NATIONALITY.resolveFor(actor);
        } else if (NATIONALITY.equalsIgnoreCase("Bolivian")) {
            optionWebElement = OPTION_BOLIVIAN_NATIONALITY.resolveFor(actor);
        } else if (NATIONALITY.equalsIgnoreCase("French")) {
            optionWebElement = OPTION_FRENCH_NATIONALITY.resolveFor(actor);
        }
        return optionWebElement;
    }
}
