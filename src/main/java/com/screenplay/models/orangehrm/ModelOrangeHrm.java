package com.screenplay.models.orangehrm;

public class ModelOrangeHrm {

    private String firstName;
    private String middleName;
    private String lastName;
    private String location;
    private String otherId;
    private String birthday;
    private String maritalStatus;
    private String gender;
    private String nationality;
    private String driverLicense;
    private String licenceExpiry;
    private String nickName;
    private String militaryService;
    private String smoke;
    private String hobby;

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLocation() {
        return location;
    }

    public String getOtherId() {
        return otherId;
    }

    public String getDateBirth() {
        return birthday;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public String getNationality() {
        return nationality;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public String getLicenceExpiry() {
        return licenceExpiry;
    }

    public String getNickName() {
        return nickName;
    }

    public String getMilitaryService() {
        return militaryService;
    }

    public String getSmoke() {
        return smoke;
    }

    public String getHobby() {
        return hobby;
    }
}
