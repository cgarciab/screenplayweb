package com.screenplay.models.traductor;

public class ModelTranslateGoogle {

    private String languageInput;
    private String languageOutput;
    private String wordInput;

    public String getLanguageInput() {
        return languageInput;
    }

    public String getLanguageOutput() {
        return languageOutput;
    }

    public String getWordInput() {
        return wordInput;
    }
}
