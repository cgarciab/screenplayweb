package com.screenplay.models.choucair_academy;

public class ModelChoucairAcademy {

    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
