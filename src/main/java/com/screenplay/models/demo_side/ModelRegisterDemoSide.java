package com.screenplay.models.demo_side;

public class ModelRegisterDemoSide {

    private String nameImg;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String phone;
    private String gender;
    private String hobby;
    private String language;
    private String skills;
    private String country;
    private String continent;
    private String dateOfBirth;
    private String password;
    private String yearOfBirth;
    private String monthOfBirth;
    private String dayOfBirth;

    public String getNameImg() {
        return nameImg;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getGender() {
        return gender;
    }

    public String getHobby() {
        return hobby;
    }

    public String getLanguage() {
        return language;
    }

    public String getSkills() {
        return skills;
    }

    public String getCountry() {
        return country;
    }

    public String getContinent() {
        return continent;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public String getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(String yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getMonthOfBirth() {
        return monthOfBirth;
    }

    public void setMonthOfBirth(String monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
    }

    public String getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(String dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }
}