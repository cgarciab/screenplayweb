package com.screenplay.models.herokuapp;

public class ModelHerokuAppointment {

    private String date;
    private String documentNumDoctor;
    private String documentNumPatient;
    private String comments;

    public String getDate() {
        return date;
    }

    public String getDocumentNumDoctor() {
        return documentNumDoctor;
    }

    public String getDocumentNumPatient() {
        return documentNumPatient;
    }

    public String getComments() {
        return comments;
    }
}
