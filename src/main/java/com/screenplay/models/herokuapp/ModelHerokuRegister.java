package com.screenplay.models.herokuapp;

public class ModelHerokuRegister {

    private String firstName;
    private String lastName;
    private String phone;
    private String documentType;
    private String documentNum;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getDocumentNum() {
        return documentNum;
    }
}
