package com.screenplay.userinterfaces.traductor;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsTranslateGoogle {
    public static final Target OPTION_LIST_LEFT = Target.the("option List left languages").locatedBy("//div[@class='sl-more tlid-open-source-language-list']");
    public static final Target OPTION_LIST_RIGHT = Target.the("option List right languages").locatedBy(" //div[@class='tl-more tlid-open-target-language-list']");
    public static final Target LANGUAGES_OPTION_LEFT = Target.the("language list").locatedBy("//div[contains(@class,'item_icon sl_list_')]//following-sibling::div");
    public static final Target LANGUAGES_OPTION_RIGHT = Target.the("language list").locatedBy("//div[contains(@class,'item_icon tl_list_')]//following-sibling::div");
    public static final Target INPUT_WORD = Target.the("Input Word").locatedBy("//textarea[@id='source']");
    public static final Target OUTPUT_WORD = Target.the("Output Word").locatedBy("//span[@class='tlid-translation translation']/span");
}
