package com.screenplay.userinterfaces.traductor;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://translate.google.com/?hl=es")
public class PageObjectTranslateGoogle extends PageObject {
}
