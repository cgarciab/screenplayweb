package com.screenplay.userinterfaces.herokuapp;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsHerokuappRegisterPatient {
    public static final Target BTN_REGISTER_PATIENT = Target.the("Button register patient").locatedBy("//a[@href='addPatient']");
    public static final Target INPUT_FIRSTNAME = Target.the("Input first name").locatedBy("//input[@name='name']");
    public static final Target INPUT_LASTNAME = Target.the("Input last name").locatedBy("//input[@name='last_name']");
    public static final Target INPUT_PHONE = Target.the("Input telephone").locatedBy("//input[@name='telephone']");
    public static final Target CBOX_DOCUMENT_TYPE = Target.the("ComboBox identification type").locatedBy("//select[@name='identification_type']");
    public static final Target INPUT_DOCUMENT_NUMBER = Target.the("Input identification number").locatedBy("//input[@name='identification']");
    public static final Target RDBTN_HEALTH = Target.the("Radio button Prepaid Health").locatedBy("//input[@name='prepaid']");
    public static final Target BTN_SAVE = Target.the("Button save data doctor").locatedBy("//a[@class='btn btn-primary pull-right']");
}
