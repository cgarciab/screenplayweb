package com.screenplay.userinterfaces.herokuapp;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsHerokuappAppointment {
    public static final Target BTN_APPOINTMENT_SCHEDULING = Target.the("Boton appointment scheduling").locatedBy("//a[@href='appointmentScheduling']");
    public static final Target INPUT_DATE_APPOINTMENT = Target.the("Input date appointment").locatedBy("//input[@id='datepicker']");
    public static final Target INPUT_NUM_PATIENT = Target.the("Input document num patient").locatedBy("(//input[@class='form-control'])[1]");
    public static final Target INPUT_NUM_DOCTOR = Target.the("Input document num doctor").locatedBy("(//input[@class='form-control'])[2]");
    public static final Target INPUT_COMMENTS = Target.the("Input comments").locatedBy("//textarea[@class='form-control']");
    public static final Target BTN_SAVE = Target.the("Button save data doctor").locatedBy("//a[@class='btn btn-primary pull-right']");
}