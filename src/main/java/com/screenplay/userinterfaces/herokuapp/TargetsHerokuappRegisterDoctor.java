package com.screenplay.userinterfaces.herokuapp;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsHerokuappRegisterDoctor {
    public static final Target BTN_REGISTER_DOCTOR = Target.the("Button register doctor").locatedBy("//a[@href='addDoctor']");
    public static final Target INPUT_FIRSTNAME = Target.the("Input first name").locatedBy("//input[@id='name']");
    public static final Target INPUT_LASTNAME = Target.the("Input last name").locatedBy("//input[@id='last_name']");
    public static final Target INPUT_TELEPHONE = Target.the("Input telephone").locatedBy("//input[@id='telephone']");
    public static final Target CBOX_DOCUMENT_TYPE = Target.the("Input identification type").locatedBy("//select[@id='identification_type']");
    public static final Target INPUT_DOCUMENT_NUMBER = Target.the("Input identification number").locatedBy("//input[@id='identification']");
    public static final Target BTN_SAVE = Target.the("Button save data doctor").locatedBy("//a[@class='btn btn-primary pull-right']");
    public static final Target LBL_TITLE_SAVE = Target.the("Label Title save").locatedBy("//h3[@class='panel-title']");
}
