package com.screenplay.userinterfaces.choucair_academy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class TargetsChoucairLogin extends PageObject {
    public static final Target BTN_SIGN_IN = Target.the("Button sign in").locatedBy("(//strong[contains(.,'Ingresar')])[1]");
    public static final Target INPUT_USER = Target.the("Input user").locatedBy("//input[@id='username']");
    public static final Target INPUT_PASSWORD = Target.the("Input password").locatedBy("//input[@id='password']");
    public static final Target BTN_ACCESS = Target.the("Button access").locatedBy("//button[@type='submit'][@class='btn btn-primary']");
}
