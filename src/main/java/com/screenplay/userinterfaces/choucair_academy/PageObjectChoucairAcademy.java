package com.screenplay.userinterfaces.choucair_academy;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://operacion.choucairtesting.com/academy/login/index.php")
public class PageObjectChoucairAcademy extends PageObject {
}
