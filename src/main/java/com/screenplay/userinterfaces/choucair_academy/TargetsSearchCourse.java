package com.screenplay.userinterfaces.choucair_academy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TargetsSearchCourse extends PageObject {
    public static final Target BTN_UNIVERSITY = Target.the("Select University Choucair").located(By.xpath("//div[@id='universidad']//strong"));
    public static final Target INPUT_COURSE = Target.the("Input course").located(By.xpath("//input[@id='coursesearchbox']"));
    public static final Target BTN_SEARCH = Target.the("Button findTheCourse search").located(By.xpath("//button[contains(.,'Ir')]"));
    public static final Target SELECT_COURSE = Target.the("Select course").located(By.xpath("//i[@class='fa fa-arrow-circle-right']"));
    public static final Target LBL_TITLE_COURSE = Target.the("Title course").located(By.xpath("//div[@class='page-header-headings']/h1"));
}

