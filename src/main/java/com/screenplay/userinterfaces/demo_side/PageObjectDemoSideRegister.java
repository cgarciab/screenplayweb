package com.screenplay.userinterfaces.demo_side;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class PageObjectDemoSideRegister extends PageObject {
}
