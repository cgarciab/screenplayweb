package com.screenplay.userinterfaces.demo_side;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsDemoSideRegister {
    public static final Target INPUT_FIRST_NAME = Target.the("Input first name").locatedBy("//input[@placeholder='First Name']");
    public static final Target INPUT_LAST_NAME = Target.the("Input last name").locatedBy("//input[@placeholder='Last Name']");
    public static final Target INPUT_ADDRESS = Target.the("Input address").locatedBy("//textarea[@rows='3']");
    public static final Target INPUT_EMAIL = Target.the("Input address email").locatedBy("//input[@type='email']");
    public static final Target INPUT_PHONE = Target.the("Input phone").locatedBy("//input[@type='tel']");
    public static final Target RDBTN_GENDER = Target.the("Radio Button gender").locatedBy("//input[@name='radiooptions']");
    public static final Target RDBTN_HOBBIES = Target.the("Radio Button hobbies").locatedBy("//input[contains(@id,'checkbox')]");

    public static final Target INPUT_LANGUAGES = Target.the("Input languages").locatedBy("//div[@id='msdd']");
    public static final Target OPT_LANGUAGE = Target.the("Option language").locatedBy("//a[@class='ui-corner-all']");
    public static final Target CBOX_SKILLS = Target.the("ComboBox skills").locatedBy("//select[@id='Skills']");
    public static final Target CBOX_COUNTRY = Target.the("ComboBox country").locatedBy("//select[@id='countries']");
    public static final Target CBOX_CONTINENT = Target.the("ComboBox continent").locatedBy("//span[@class='select2-selection__arrow']");
    public static final Target INPUT_CONTINENT = Target.the("Input continent japan").locatedBy("//input[@class='select2-search__field']");
    public static final Target CBOX_YEAR = Target.the("ComboBox year").locatedBy("//select[@id='yearbox']");
    public static final Target CBOX_MONTH = Target.the("ComboBox month").locatedBy("//select[@placeholder='Month']");
    public static final Target CBOX_DAY = Target.the("ComboBox day").locatedBy("//select[@id='daybox']");
    public static final Target INPUT_PASSWORD = Target.the("Input password").locatedBy("//input[@id='firstpassword']");
    public static final Target INPUT_PASSWORD_CHECK = Target.the("Input password check").locatedBy("//input[@id='secondpassword']");
    public static final Target BTN_SUBMIT = Target.the("Button submit register").locatedBy("//button[@id='submitbtn']");
    public static final Target TAG_WEB_TABLE = Target.the("Tag the page").locatedBy("//li[@class='active']/child::a[@href='WebTable.html']");
}
