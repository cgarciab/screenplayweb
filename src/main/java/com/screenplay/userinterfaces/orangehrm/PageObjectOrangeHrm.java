package com.screenplay.userinterfaces.orangehrm;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class PageObjectOrangeHrm extends PageObject {
}
