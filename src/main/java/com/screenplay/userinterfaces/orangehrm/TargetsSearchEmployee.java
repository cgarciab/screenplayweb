package com.screenplay.userinterfaces.orangehrm;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsSearchEmployee {
    //Search new employee
    public static final Target LBL_ATTACHMENTS = Target.the("Details attachments").locatedBy("//span[contains(.,'Attachments')]");
    public static final Target BTN_EMPLOYEE = Target.the("Button Employee list").locatedBy("//span[contains(.,'Employee List')]");
    public static final Target INPUT_SEARCH = Target.the("Search engine").locatedBy("//input[@id='employee_name_quick_filter_employee_list_value']");
    public static final Target BTN_SEARCH = Target.the("Button search").locatedBy("//i[@id='quick_search_icon']");
    public static final Target FIELD_EMPLOYEE = Target.the("Field employee").locatedBy("//td[@class='cursor-pointer'][2]");

}
