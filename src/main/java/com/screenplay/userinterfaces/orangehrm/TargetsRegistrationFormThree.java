package com.screenplay.userinterfaces.orangehrm;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsRegistrationFormThree {

    //form three full data
    public static final Target LBL_DETAILS = Target.the("Title details").locatedBy("//h4[contains(.,'Employment Details')]");

    public static final Target CBOX_REGION = Target.the("ComboBox region").locatedBy("(//input[contains(@class,'select-dropdown')])[6]");
    public static final Target OPTION_REGION_2 = Target.the("Option region Region-2").locatedBy("//span[contains(.,'Region-2')]");

    public static final Target CBOX_FTE = Target.the("ComboBox FTE").locatedBy("(//input[contains(@class,'select-dropdown')])[7]");
    public static final Target OPTION_FTE_075 = Target.the("Option FTE 0.75").locatedBy("//span[contains(.,'0.75')]");

    public static final Target CBOX_DEPARTMENT = Target.the("ComboBox department").locatedBy("(//input[contains(@class,'select-dropdown')])[8]");
    public static final Target OPTION_DEPARTMENT_SUBUNIT2 = Target.the("Option department Sub unit-2").locatedBy("//span[contains(.,'Sub unit-2')]");

}
