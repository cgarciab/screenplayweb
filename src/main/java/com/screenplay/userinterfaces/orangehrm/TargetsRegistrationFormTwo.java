package com.screenplay.userinterfaces.orangehrm;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsRegistrationFormTwo {
    //form two full data
    public static final Target INPUT_OTHER_ID = Target.the("Input otherId").locatedBy("//input[@id='otherId']");
    public static final Target INPUT_DATE_BIRTH = Target.the("Input dateBirth").locatedBy("//input[@id='emp_birthday']");

    public static final Target CBOX_MARITAL_STATUS = Target.the("ComboBox maritalStatus").locatedBy("(//input[contains(@class,'select-dropdown')])[1]");
    public static final Target OPTION_MARRIED_MARITAL = Target.the("Option maritalStatus Married").locatedBy("//span[contains(.,'Married')]");
    public static final Target OPTION_SINGLE_MARITAL = Target.the("Option maritalStatus Single").locatedBy("//span[contains(.,'Single')]");
    public static final Target OPTION_OTHER_MARITAL = Target.the("Option maritalStatus Other").locatedBy("//span[contains(.,'Other')]");

    public static final Target CBOX_GENDER = Target.the("ComboBox gender").locatedBy("(//input[contains(@class,'select-dropdown')])[2]");
    public static final Target OPTION_GENDER_FEMALE = Target.the("Option gender Female").locatedBy("//span[contains(.,'Female')]");
    public static final Target OPTION_GENDER_MALE = Target.the("Option gender Male").locatedBy("//span[contains(.,'Male')]");
    public static final Target OPTION_GENDER_NONBINARY = Target.the("Option gender Non-Binary").locatedBy("//span[contains(.,'Non-Binary')]");

    public static final Target CBOX_NATIONALITY = Target.the("ComboBox nationality").locatedBy("(//input[contains(@class,'select-dropdown')])[3]");
    public static final Target OPTION_AMERICAN_NATIONALITY = Target.the("Option American nationality ").locatedBy("//span[contains(.,'American')]");
    public static final Target OPTION_ARGENTINEAN_NATIONALITY = Target.the("Option Argentinean nationality ").locatedBy("//span[contains(.,'Argentinean')]");
    public static final Target OPTION_AUSTRALIAN_NATIONALITY = Target.the("Option Australian nationality ").locatedBy("//span[contains(.,'Australian')]");
    public static final Target OPTION_BOLIVIAN_NATIONALITY = Target.the("Option Bolivian nationality ").locatedBy("//span[contains(.,'Bolivian')]");
    public static final Target OPTION_FRENCH_NATIONALITY = Target.the("Option French nationality ").locatedBy("//span[contains(.,'French')]");

    public static final Target INPUT_DRIVER_LICENSE = Target.the("Input driverLicense").locatedBy("//input[@id='licenseNo']");
    public static final Target INPUT_LICENCE_EXPIRY = Target.the("Input licenceExpiry").locatedBy("//input[@id='emp_dri_lice_exp_date']");
    public static final Target INPUT_NICKNAME = Target.the("Input nickName").locatedBy("//input[@id='nickName']");
    public static final Target INPUT_MILITARY_SERVICE = Target.the("Input militaryService").locatedBy("//input[@id='militaryService']");
    public static final Target CCKBOX_SMOKE = Target.the("Checkbox smoke").locatedBy("//label[@for='smoker']");

    public static final Target CBOX_BLOOD_GROUP = Target.the("ComboBox bloodGroup").locatedBy("(//input[contains(@class,'select-dropdown')])[4]");
    public static final Target OPTION_BLOOD_GROUP_A = Target.the("Option bloodGroup A").locatedBy("(//span[contains(.,'A')])[47]");
    public static final Target OPTION_BLOOD_GROUP_AB = Target.the("Option bloodGroup AB").locatedBy("//span[contains(.,'AB')]");
    public static final Target OPTION_BLOOD_GROUP_B = Target.the("Option bloodGroup B").locatedBy("(//span[contains(.,'B')])[32]");
    public static final Target OPTION_BLOOD_GROUP_O = Target.the("Option bloodGroup O").locatedBy("(//span[contains(.,'O')])[13]");

    public static final Target INPUT_HOBBY = Target.the("Input hobby").locatedBy("//input[@id='5']");
    public static final Target BTN_SAVE = Target.the("Button next and save").locatedBy("//button[@class='btn waves-effect waves-light right']");

}
