package com.screenplay.userinterfaces.orangehrm;

import net.serenitybdd.screenplay.targets.Target;

public class TargetsRegistrationFormOne {
    //search add employee
    public static final Target BTN_LOGIN = Target.the("Button login").locatedBy("//input[@id='btnLogin']");
    public static final Target BTN_PIM = Target.the("Button PIM").locatedBy("//span[contains(.,'PIM')]");
    //form one basic data
    public static final Target BTN_ADD_EMPLOYEE = Target.the("Button add employee").locatedBy("(//span[contains(.,'Add Employee')])[2]");
    public static final Target INPUT_FIRSTNAME = Target.the("Input firstname").locatedBy("//input[@id='firstName']");
    public static final Target INPUT_MIDDLE_NAME = Target.the("Input middleName").locatedBy("//input[@id='middleName']");
    public static final Target INPUT_LASTNAME = Target.the("Input lastName").locatedBy("//input[@id='lastName']");
    public static final Target CBOX_LOCATION = Target.the("ComboBox location").locatedBy("(//input[@class='select-dropdown'])[2]");
    public static final Target OPTION_CANADIAN_LOCATION = Target.the("Option Canadian location ").locatedBy("//span[contains(.,'Canadian Development Center')]");
    public static final Target OPTION_AUSTRALIAN_LOCATION = Target.the("Option australian location ").locatedBy("//span[contains(.,'Australian Regional HQ')]");
    public static final Target OPTION_BANGLADESH_LOCATION = Target.the("Option bangladesh location ").locatedBy("(//span[contains(.,'Dhaka')])[1]");
    public static final Target OPTION_CHINA_LOCATION = Target.the("Option China location ").locatedBy("//span[contains(.,'Chinese Development Center')]");
    public static final Target BTN_NEXT = Target.the("Button next").locatedBy("//a[@id='systemUserSaveBtn']");
}
