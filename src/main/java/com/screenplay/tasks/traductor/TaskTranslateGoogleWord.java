package com.screenplay.tasks.traductor;

import com.screenplay.interactions.traductor.SelectLanguageOfTheList;
import com.screenplay.models.traductor.ModelTranslateGoogle;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static com.screenplay.userinterfaces.traductor.TargetsTranslateGoogle.*;

public class TaskTranslateGoogleWord implements Task {

    private final List<ModelTranslateGoogle> DATA;

    public TaskTranslateGoogleWord(List<ModelTranslateGoogle> data) {
        this.DATA = data;
    }

    public static Performable translateWord(List<ModelTranslateGoogle> data) {
        return Tasks.instrumented(TaskTranslateGoogleWord.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(OPTION_LIST_LEFT),
                SelectLanguageOfTheList.selectItem(LANGUAGES_OPTION_LEFT.resolveAllFor(actor), DATA.get(0).getLanguageInput()),
                Click.on(OPTION_LIST_RIGHT),
                SelectLanguageOfTheList.selectItem(LANGUAGES_OPTION_RIGHT.resolveAllFor(actor), DATA.get(0).getLanguageOutput()),
                Enter.theValue(DATA.get(0).getWordInput()).into(INPUT_WORD)
        );
    }
}
