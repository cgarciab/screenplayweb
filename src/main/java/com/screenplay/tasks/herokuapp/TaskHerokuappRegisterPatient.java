package com.screenplay.tasks.herokuapp;

import com.screenplay.models.herokuapp.ModelHerokuRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static com.screenplay.userinterfaces.herokuapp.TargetsHerokuappRegisterPatient.*;

public class TaskHerokuappRegisterPatient implements Task {

    private final List<ModelHerokuRegister> DATA;

    public TaskHerokuappRegisterPatient(List<ModelHerokuRegister> DATA) {
        this.DATA = DATA;
    }

    public static Performable enterData(List<ModelHerokuRegister> data) {
        return Tasks.instrumented(TaskHerokuappRegisterPatient.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_REGISTER_PATIENT),
                Enter.theValue(DATA.get(0).getFirstName()).into(INPUT_FIRSTNAME),
                Enter.theValue(DATA.get(0).getLastName()).into(INPUT_LASTNAME),
                Enter.theValue(DATA.get(0).getPhone()).into(INPUT_PHONE),
                SelectFromOptions.byVisibleText(DATA.get(0).getDocumentType()).from(CBOX_DOCUMENT_TYPE),
                Enter.theValue(DATA.get(0).getDocumentNum()).into(INPUT_DOCUMENT_NUMBER),
                Click.on(RDBTN_HEALTH),
                Click.on(BTN_SAVE)
        );
    }
}
