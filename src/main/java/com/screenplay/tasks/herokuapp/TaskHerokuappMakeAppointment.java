package com.screenplay.tasks.herokuapp;

import com.screenplay.models.herokuapp.ModelHerokuAppointment;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static com.screenplay.userinterfaces.herokuapp.TargetsHerokuappAppointment.*;

public class TaskHerokuappMakeAppointment implements Task {

    private final List<ModelHerokuAppointment> DATA;

    public TaskHerokuappMakeAppointment(List<ModelHerokuAppointment> data) {
        this.DATA = data;
    }

    public static Performable makeAnAppointment(List<ModelHerokuAppointment> data) {
        return Tasks.instrumented(TaskHerokuappMakeAppointment.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_APPOINTMENT_SCHEDULING),
                Enter.theValue(DATA.get(0).getDate()).into(INPUT_DATE_APPOINTMENT),
                Enter.theValue(DATA.get(0).getDocumentNumDoctor()).into(INPUT_NUM_DOCTOR),
                Enter.theValue(DATA.get(0).getDocumentNumPatient()).into(INPUT_NUM_PATIENT),
                Enter.theValue(DATA.get(0).getComments()).into(INPUT_COMMENTS),
                Click.on(BTN_SAVE)
        );
    }
}
