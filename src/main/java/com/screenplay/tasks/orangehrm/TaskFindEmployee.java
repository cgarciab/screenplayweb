package com.screenplay.tasks.orangehrm;

import com.screenplay.interactions.orangehrm.WaitAWhile;
import com.screenplay.models.orangehrm.ModelOrangeHrm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static com.screenplay.userinterfaces.orangehrm.TargetsSearchEmployee.*;

public class TaskFindEmployee implements Task {

    private final List<ModelOrangeHrm> DATA;

    public TaskFindEmployee(List<ModelOrangeHrm> DATA) {
        this.DATA = DATA;
    }

    public static Performable findEmployee(List<ModelOrangeHrm> data) {
        return Tasks.instrumented(TaskFindEmployee.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_EMPLOYEE),
                Click.on(INPUT_SEARCH),
                Enter.theValue(DATA.get(0).getFirstName() + " " + DATA.get(0).getMiddleName() + " " + DATA.get(0).getLastName()).into(INPUT_SEARCH),
                Click.on(BTN_SEARCH),
                WaitAWhile.waitSeconds(3)
        );
    }
}
