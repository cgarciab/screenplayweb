package com.screenplay.tasks.orangehrm;

import com.screenplay.interactions.orangehrm.*;
import com.screenplay.models.orangehrm.ModelOrangeHrm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormOne.*;
import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormThree.*;
import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormTwo.*;
import static com.screenplay.userinterfaces.orangehrm.TargetsSearchEmployee.LBL_ATTACHMENTS;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class TaskRegisterEmployee implements Task {

    private final List<ModelOrangeHrm> DATA;

    public TaskRegisterEmployee(List<ModelOrangeHrm> DATA) {
        this.DATA = DATA;
    }

    public static Performable registerEmployee(List<ModelOrangeHrm> data) {
        return Tasks.instrumented(TaskRegisterEmployee.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(BTN_PIM),
                Click.on(BTN_ADD_EMPLOYEE)
        );

        actor.attemptsTo(
                WaitAWhile.waitSeconds(7),
                WaitUntil.the(INPUT_FIRSTNAME, isEnabled()),
                Enter.theValue(DATA.get(0).getFirstName()).into(INPUT_FIRSTNAME),
                Enter.theValue(DATA.get(0).getMiddleName()).into(INPUT_MIDDLE_NAME),
                Enter.theValue(DATA.get(0).getLastName()).into(INPUT_LASTNAME),

                Click.on(CBOX_LOCATION),
                SelectLocationComboBox.selectOptionLocation(DATA.get(0).getLocation()),

                Click.on(BTN_NEXT)
        );

        actor.attemptsTo(
                WaitAWhile.waitSeconds(7),
                WaitUntil.the(INPUT_OTHER_ID, isEnabled()),
                Enter.theValue(DATA.get(0).getOtherId()).into(INPUT_OTHER_ID),
                Enter.theValue(DATA.get(0).getDateBirth()).into(INPUT_DATE_BIRTH),

                Click.on(CBOX_MARITAL_STATUS),
                SelectMaritalStatusComboBox.selectMaritalStatus(DATA.get(0).getMaritalStatus()),

                Click.on(CBOX_GENDER),
                SelectGenderComboBox.selectGender(DATA.get(0).getGender()),

                Click.on(CBOX_NATIONALITY),
                SelectNationalityComboBox.selectNationality(DATA.get(0).getNationality()),

                Enter.theValue(DATA.get(0).getDriverLicense()).into(INPUT_DRIVER_LICENSE),
                Enter.theValue(DATA.get(0).getLicenceExpiry()).into(INPUT_LICENCE_EXPIRY),
                Enter.theValue(DATA.get(0).getNickName()).into(INPUT_NICKNAME),
                Enter.theValue(DATA.get(0).getMilitaryService()).into(INPUT_MILITARY_SERVICE),
                SelectSmokeCheckBox.selectOption(DATA.get(0).getSmoke())
        );

        actor.attemptsTo(
                Click.on(CBOX_BLOOD_GROUP),
                Click.on(OPTION_BLOOD_GROUP_A),
                Enter.theValue(DATA.get(0).getHobby()).into(INPUT_HOBBY),
                Click.on(BTN_SAVE)
        );

        actor.attemptsTo(
                WaitAWhile.waitSeconds(7),

                WaitUntil.the(LBL_DETAILS, isEnabled()),

                Click.on(CBOX_REGION),
                Click.on(OPTION_REGION_2),

                Click.on(CBOX_FTE),
                Click.on(OPTION_FTE_075),

                Click.on(CBOX_DEPARTMENT),
                Click.on(OPTION_DEPARTMENT_SUBUNIT2),

                Click.on(BTN_SAVE)
        );

        actor.attemptsTo(
                WaitAWhile.waitSeconds(7),
                WaitUntil.the(LBL_ATTACHMENTS, isVisible())
        );
    }
}
