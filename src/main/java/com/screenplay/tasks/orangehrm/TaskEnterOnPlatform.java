package com.screenplay.tasks.orangehrm;

import com.screenplay.userinterfaces.orangehrm.PageObjectOrangeHrm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

import static com.screenplay.userinterfaces.orangehrm.TargetsRegistrationFormOne.BTN_LOGIN;

public class TaskEnterOnPlatform implements Task {

    private PageObjectOrangeHrm pageObjectOrangeHrm;

    public static Performable enterOn() {
        return Tasks.instrumented(TaskEnterOnPlatform.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(pageObjectOrangeHrm),
                Click.on(BTN_LOGIN)
        );
    }
}
