package com.screenplay.tasks.choucair_academy;

import com.screenplay.userinterfaces.choucair_academy.TargetsSearchCourse;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class TaskSearchCourse implements Task {

    private final String COURSE;

    public TaskSearchCourse(String COURSE) {
        this.COURSE = COURSE;
    }

    public static Performable findTheCourse(String course) {
        return Tasks.instrumented(TaskSearchCourse.class, course);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TargetsSearchCourse.BTN_UNIVERSITY),
                Enter.theValue(COURSE).into(TargetsSearchCourse.INPUT_COURSE),
                Click.on(TargetsSearchCourse.BTN_SEARCH),
                Click.on(TargetsSearchCourse.SELECT_COURSE)
        );
    }
}
