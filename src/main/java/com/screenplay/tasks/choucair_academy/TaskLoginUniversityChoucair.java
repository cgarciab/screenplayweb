package com.screenplay.tasks.choucair_academy;

import com.screenplay.models.choucair_academy.ModelChoucairAcademy;
import com.screenplay.userinterfaces.choucair_academy.TargetsChoucairLogin;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

public class TaskLoginUniversityChoucair implements Task {

    private final List<ModelChoucairAcademy> DATA;

    public TaskLoginUniversityChoucair(List<ModelChoucairAcademy> data) {
        this.DATA = data;
    }

    public static TaskLoginUniversityChoucair signIn(List<ModelChoucairAcademy> data) {
        return Tasks.instrumented(TaskLoginUniversityChoucair.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(TargetsChoucairLogin.BTN_SIGN_IN),
                Enter.theValue(DATA.get(0).getUserName()).into(TargetsChoucairLogin.INPUT_USER),
                Enter.theValue(DATA.get(0).getPassword()).into(TargetsChoucairLogin.INPUT_PASSWORD),
                Click.on(TargetsChoucairLogin.BTN_ACCESS)
        );
    }
}
