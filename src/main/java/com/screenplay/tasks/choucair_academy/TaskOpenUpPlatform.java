package com.screenplay.tasks.choucair_academy;

import com.screenplay.userinterfaces.choucair_academy.PageObjectChoucairAcademy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class TaskOpenUpPlatform implements Task {

    private PageObjectChoucairAcademy pageObjectChoucairAcademy;

    public static Performable enterThePage() {
        return Tasks.instrumented(TaskOpenUpPlatform.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(pageObjectChoucairAcademy));
    }
}
