package com.screenplay.tasks.demo_side;

import com.screenplay.interactions.demo_side.SelectHobbyRadioButton;
import com.screenplay.interactions.demo_side.SelectLanguageListComboBox;
import com.screenplay.interactions.demo_side.UpdateFileImg;
import com.screenplay.models.demo_side.ModelRegisterDemoSide;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import org.openqa.selenium.Keys;

import java.util.List;

import static com.screenplay.userinterfaces.demo_side.TargetsDemoSideRegister.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class TaskFillInTheForm implements Task {

    private final List<ModelRegisterDemoSide> DATA;

    public TaskFillInTheForm(List<ModelRegisterDemoSide> DATA) {
        this.DATA = DATA;
    }

    public static Performable fillInTheFields(List<ModelRegisterDemoSide> data) {
        return instrumented(TaskFillInTheForm.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        this.setSplitDateOfBirth();

        actor.attemptsTo(
                UpdateFileImg.fileInput(DATA.get(0).getNameImg())
        );
        actor.attemptsTo(
                Enter.theValue(DATA.get(0).getFirstName()).into(INPUT_FIRST_NAME),
                Enter.theValue(DATA.get(0).getLastName()).into(INPUT_LAST_NAME),
                Enter.theValue(DATA.get(0).getAddress()).into(INPUT_ADDRESS),
                Enter.theValue(DATA.get(0).getEmail()).into(INPUT_EMAIL),
                Enter.theValue(DATA.get(0).getPhone()).into(INPUT_PHONE)
        );
        actor.attemptsTo(
                SelectHobbyRadioButton.selectItem(RDBTN_GENDER.resolveAllFor(actor), DATA.get(0).getGender())
        );
        actor.attemptsTo(
                SelectHobbyRadioButton.selectItem(RDBTN_HOBBIES.resolveAllFor(actor), DATA.get(0).getHobby())
        );
        actor.attemptsTo(
                Click.on(INPUT_LANGUAGES),
                SelectLanguageListComboBox.selectItem(OPT_LANGUAGE.resolveAllFor(actor), DATA.get(0).getLanguage())
        );
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(DATA.get(0).getSkills()).from(CBOX_SKILLS),
                SelectFromOptions.byVisibleText(DATA.get(0).getCountry()).from(CBOX_COUNTRY)
        );
        actor.attemptsTo(
                Click.on(CBOX_CONTINENT),
                Enter.theValue(DATA.get(0).getContinent()).into(INPUT_CONTINENT).thenHit(Keys.ENTER)
        );
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(DATA.get(0).getYearOfBirth()).from(CBOX_YEAR),
                SelectFromOptions.byVisibleText(DATA.get(0).getMonthOfBirth()).from(CBOX_MONTH),
                SelectFromOptions.byVisibleText(DATA.get(0).getDayOfBirth()).from(CBOX_DAY)
        );
        actor.attemptsTo(
                Enter.theValue(DATA.get(0).getPassword()).into(INPUT_PASSWORD),
                Enter.theValue(DATA.get(0).getPassword()).into(INPUT_PASSWORD_CHECK),
                Click.on(BTN_SUBMIT)
        );
    }

    private void setSplitDateOfBirth() {
        String[] birthday = DATA.get(0).getDateOfBirth().split("-");
        DATA.get(0).setYearOfBirth(birthday[0]);
        DATA.get(0).setMonthOfBirth(birthday[1]);
        DATA.get(0).setDayOfBirth(birthday[2]);
    }
}
