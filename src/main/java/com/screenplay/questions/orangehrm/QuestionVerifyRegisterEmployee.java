package com.screenplay.questions.orangehrm;

import com.screenplay.models.orangehrm.ModelOrangeHrm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

import static com.screenplay.userinterfaces.orangehrm.TargetsSearchEmployee.FIELD_EMPLOYEE;

public class QuestionVerifyRegisterEmployee implements Question<Boolean> {

    private final List<ModelOrangeHrm> DATA;

    public QuestionVerifyRegisterEmployee(List<ModelOrangeHrm> data) {
        this.DATA = data;
    }

    public static Question<Boolean> isRegistered(List<ModelOrangeHrm> data) {
        return new QuestionVerifyRegisterEmployee(data);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return FIELD_EMPLOYEE.resolveFor(actor).getText().equals(
                DATA.get(0).getFirstName() + " " + DATA.get(0).getMiddleName() + " " + DATA.get(0).getLastName()
        );
    }
}
