package com.screenplay.questions.traductor;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.screenplay.userinterfaces.traductor.TargetsTranslateGoogle.OUTPUT_WORD;

public class QuestionVerifyTranslatedWord implements Question<Boolean> {

    private final String WORD_OUTPUT;

    public QuestionVerifyTranslatedWord(String wordOutPut) {
        this.WORD_OUTPUT = wordOutPut;
    }

    public static Question<Boolean> OutputWordIs(String wordOutput) {
        return new QuestionVerifyTranslatedWord(wordOutput);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return OUTPUT_WORD.resolveFor(actor).getText().equals(WORD_OUTPUT);
    }
}
