package com.screenplay.questions.choucair_academy;

import com.screenplay.userinterfaces.choucair_academy.TargetsSearchCourse;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class QuestionVerifyEnterCourse implements Question<Boolean> {

    private final String COURSE;

    public QuestionVerifyEnterCourse(String course) {
        this.COURSE = course;
    }

    public static QuestionVerifyEnterCourse verifyTheCourse(String course) {
        return new QuestionVerifyEnterCourse(course);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String nameTitleCourse = Text.of(TargetsSearchCourse.LBL_TITLE_COURSE).viewedBy(actor).asString();
        return COURSE.equals(nameTitleCourse);
    }
}
