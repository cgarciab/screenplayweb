package com.screenplay.questions.demo_side;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.screenplay.userinterfaces.demo_side.TargetsDemoSideRegister.TAG_WEB_TABLE;

public class QuestionVerifyRegister implements Question<Boolean> {

    public static Question<Boolean> isActiveTagWebTable() {
        return new QuestionVerifyRegister();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return TAG_WEB_TABLE.resolveFor(actor).isVisible();
    }
}
