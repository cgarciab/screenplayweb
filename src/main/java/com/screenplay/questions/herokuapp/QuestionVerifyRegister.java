package com.screenplay.questions.herokuapp;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.screenplay.userinterfaces.herokuapp.TargetsHerokuappRegisterDoctor.LBL_TITLE_SAVE;

public class QuestionVerifyRegister implements Question<Boolean> {

    private final String STATUS;

    public QuestionVerifyRegister(String status) {
        this.STATUS = status;
    }

    public static Question<Boolean> isSaved(String status) {
        return new QuestionVerifyRegister(status);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return LBL_TITLE_SAVE.resolveFor(actor).getText().equals(STATUS);
    }
}
